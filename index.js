
/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	


/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

let	firstName = "First Name: John";
let lastName = "Last Name: Smith"
let age0 = 30;
let ageNumber = "Age: " + age0;
let hobbies = ["Biking", "Mountain Climbing", "Swimming"];
let hobbyList = "Hobbies: ";
console.log(firstName);
console.log(lastName);
console.log(ageNumber);
console.log(hobbyList);
console.log(hobbies);

let workAddress = "Work Address: ";
let workAddess1 = {
	houseNumber: "32",
	street: "Washington",
	city: "Lincoln",
	state: "Nebraska",
}
console.log(workAddress);
console.log(workAddess1);

let fullName0 = "Steve Rogers";
let	fullnameFinal = "My full name is: " + fullName0;
console.log(fullnameFinal);

let agesteve = 40;
let ageFinal = "My current age is: " + agesteve;
console.log(ageFinal);

let friends = ["Tony", "Bruce", "Thor" ,"Natasha", "Clint", "Nick"];
let friendlist = "My Friends are: ";
console.log(friendlist);
console.log(friends);

let myFullProfile = "My Full Profile:"
console.log(myFullProfile);

let fullProfile = {
	username: "Captain_america",
	fullName: "Steve Rogers",
	age: 40,
	isActive: false,
}
console.log(fullProfile);
	
let bucky = "Bucky Barnes"
let buckyBarnes = "My bestfriend is: " + bucky;
console.log(buckyBarnes)

let articOcean = "Artic Ocean";
let articOceanFound = "I was found frozen in: " + articOcean;
console.log(articOceanFound)




	
